from setuptools import setup

setup(
    name='dndw',
    version='0.1',
    packages=['dndw', 'dndw.CorZu', 'dndw.ParZu', 'dndw.ParZu.core', 'dndw.ParZu.evaluation', 'dndw.ParZu.statistics',
              'dndw.ParZu.statistics.stats_creator', 'dndw.ParZu.preprocessor', 'dndw.ParZu.preprocessor.morphology',
              'dndw.ParZu.postprocessor', 'dndw.zmorge', 'dndw.zmorge.evaluation', 'dndw.veraltet_and_test'],
    url='https://git.informatik.uni-leipzig.de/jr74xaqo/ndw',
    license='',
    author='jonas',
    author_email='jm_richter+ndw@mailbox.org',
    description=''
)
