import re
from typing import Union

import flair
from flair.models import RelationExtractor

import classes
import entity_linking
from entity_linking import wikification
import i_o


class RelationExtraction:
    def __init__(self, sentence: flair.data.Sentence, pers, unwikificated:bool = False):
        self.sentence = sentence
        self.name = pers.name
        self.shorts = pers.shorts
        self.ner_span = sentence.get_spans('de-ner-large')
        self.pos_span = sentence.get_spans('de-pos')
        self.class_list = []
        self.tags = list(span.tag for span in self.ner_span)
        # print(name)
        if len(self.ner_span) > 2:
            test = ''
        for ne in self.ner_span:
            if not (any([short in ne.text for short in self.shorts])):
                wiki = wikification(ne.text, ne.tag, unwikificated=unwikificated)
                if wiki and wiki not in self.class_list:
                    self.class_list.append(wiki)
        self.replacement = False
        self.pronouns = [' er ', 'Er ', ' sie ', 'Sie ']
#        test = [short in sentence.tokenized for short in self.shorts]
        """
        Überprüfe ob eine Kurzform einer Entität oder ein Pronomen in dem Satz auftaucht.
        """
        if ((any([short in sentence.tokenized for short in self.shorts])
             or any([pronoun in sentence.tokenized for pronoun in self.pronouns]))
                and pers not in self.class_list):
            self.replacement = True
            self.class_list.append(pers)
            self.tags.append("PER")
            if len(self.class_list) > 1:
                i_o.write_repl_valid(sentence.tokenized, self.name, 'validate_replacement')
        self.class_list = list(filter(None, self.class_list))
        self.per_loc = ['PER', 'LOC']
        self.per_organ = ['PER', 'ORG']
        self.organ_loc = ['LOC', 'ORG']
        self.loc_loc = ['LOC', 'LOC']
        self.per_per = ['PER', 'PER']
        self.PER_LOC = {
            'geboren_in': ['zur Welt', 'geborene'],
            'studiert': ['Studium'],
            'Rückkehr': ['Rückkehr', 'zurückkehren'],
            'befreit': ['Befreiung'],
            'arbeitet_in': ['Landgerichtsdirektor', 'Landgerichtsrat', 'Kommandant der Invalidensiedlung', 'Direktor',
                            'tätig', 'aktivsten Köpfe der Bekennende Kirche', 'Pfarrer', 'Rechtsanwalt', 'Redakteur',
                            'Tuberkulose-Fürsorgerin', 'Superintendent', 'Finanzfachmann', 'Stadtkämmerer',
                            'Polit-Kommissar', 'Vizepräsident'],
            'kämpft': ['militärische Auseinandersetzung'],
            'Attentat': ['Anschlag'],
            'Kontakte_nach': ['Kontakt'],
            'politisches Amt': ['Vorsitzender', 'Landrat', 'Ministerpräsident', 'politischer Sekretär',
                                'Oberbürgermeister', 'Regierungsreferendar', 'Oberpräsident', 'Statdverordneter',
                                'Vorsitzender', 'Kanzler', 'minister', 'Landtagsmitglied', 'Reichstagsabgeordneter'],
            'Migration': ['emigrieren', 'verlassen', 'ausreisen', 'entkommen'],
            'lebt_in': ['Mitglied', 'überleben'],
            'stationiert_in': ['Inspekteur', 'Rittmeister', 'Militärbefehlshaber', 'Divisionskommandeur',
                               'Chef des Stabes'],
            'Regimekritiker': ['Regimekritiker'],
            'Zwangsarbeit_in': ['Zwangsarbeit'],
            'Aufenthalt_in': ['kongress in'],
            'Flucht': ['freikommen']
        }
        self.PER_ORGAN = {
            'Mitgliedschaft': ['Kandidat', 'Mitglied', 'Mitbegründer'],
            # 'close_to': [''],
            'Leiter_von': ['geleitete', 'Vizepräsident', 'Präsident', 'Chef', 'Leiter', 'Generalsekretär'],
            'arbeitet_für': ['Redner', 'Redakteur', 'Verbindungen', 'Korrespondent', 'Mitarbeiter'],
            'aktiv_gegen': ['heftige Auseinandersetzungen'],
            'verhört_von': ['Verhöre'],
            'Festnahme': ['Festnahme', 'festnehmen'],
        }
        self.ORGAN_LOC = {

        }
        self.PER_PER = {
            'Eltern/Kind': ['Tochter', 'Sohn', 'Vater', 'Mutter', 'Kindern'],
            'Geschwister': ['Bruder', 'Schwester'],
            'Ehe/Lebenspartnerschaft': ['verheiratet'],
            'befreundet_mit': ['Freund', 'besonderer Vertrauter'],
            'Bekanntschaft': ['bekannt', 'mit', 'Kontakt'],
            'schützt': ['verstecken', 'retten'],
            'arbeitet_mit': ['Vorgesetzter', 'Chef des Stabes', 'Vermögensverwalter', 'unter',
                             'Chef der Organisationsabteilung', 'Mitarbeiter', 'Vertriebsleiter', 'unter dem',
                             'persönlicher Referent', 'persönlicher Adjutant', 'Pressereferent'],
            'aktiv_gegen': ['Widerstand', 'Staatsstreich', 'heftige Auseinandersetzungen'],
            'Mitverschwörer': ['für seine Verschwörungspläne gewinnen', 'Staatsstreichs überzeugen']
        }
        self.LOC_LOC = {
            'in': ['in']
        }

    def relation_extraction(self):
        if len(self.class_list) > 1:
            """
            Falls mehr als eine Entität gefunden wurde, suche zuerst die Relationen zwischen diesen Entitäten.
            """
            rel = None
            relations = [span.text for span in self.pos_span if span.tag in ['VVFIN', 'VVPP', 'PTKVZ']]
            if set(self.tags) == set(self.per_loc):
                for key, value in self.PER_LOC.items():
                    if any(token in self.sentence.tokenized for token in value):
                        relations.append(key)
            if set(self.tags) == set(self.per_per):
                for key, value in self.PER_PER.items():
                    if any(token in self.sentence.tokenized for token in value):
                        relations.append(key)
            if set(self.tags) == set(self.per_organ):
                for key, value in self.PER_ORGAN.items():
                    if any(token in self.sentence.tokenized for token in value):
                        relations.append(key)
            if set(self.tags) == set(self.organ_loc):
                for key, value in self.ORGAN_LOC.items():
                    if any(token in self.sentence.tokenized for token in value):
                        relations.append(key)
            if set(self.tags) == set(self.loc_loc):
                for key, value in self.LOC_LOC.items():
                    if any(token in self.sentence.tokenized for token in value):
                        relations.append(key)

            if len(self.class_list) > 2:
                """
                Falls mehr als 2 Entitäten in einem Satz gefunden wurden,
                stelle für jede Person eine Relation mit allen anderen Entitäten her.
                """
                names = [c.name for c in self.class_list]
                i_o.write_repl_valid(self.sentence.tokenized, "+".join(names), 'nary_relations')
                for ent in self.class_list:
                    if ent is classes.Person:
                        pers = self.class_list.pop(self.class_list.index(ent))
                        for en in self.class_list:
                            rel = classes.Relation(
                                ent_1=pers,
                                ent_2=self.class_list[self.class_list.index(en)],
                                source=self.sentence.tokenized,
                                rel_type=relations,
                                date=find_date(self.sentence))
                            entity_linking.relationship_collection[self.name].append(rel)

            else:
                rel = classes.Relation(
                    ent_1=self.class_list[0],
                    ent_2=self.class_list[1],
                    source=self.sentence.tokenized,
                    rel_type=relations,
                    date=find_date(self.sentence))
                entity_linking.relationship_collection[self.name].append(rel)

            return rel

def find_date(sentence) -> Union[list[str], str]:
    """Sucht verschiedene Datums-Pattern"""
    # 1. Juli 1920; 1. Juli
    DATE_PATTERN_1 = re.compile(
        r'\d{1,2}\.?\s*(?:Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)\w*(?:\s*\d{4}|\d{2})?',
        re.IGNORECASE)
    # 1920; 1920/21
    DATE_PATTERN_2 = re.compile(r'1\d{3}(?:/1\d{1,3}|/\d{2})?')
    # Juli 1920; Juli 20
    DATE_PATTERN_3 = re.compile(
        r'(?:Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)\s+(?:1\d{3}|\d{2})(?:/1\d{1,3}|/\d{2})?',
        re.IGNORECASE)
    # gefundene Datumsangaben mit Tag versehen
    date_match = re.findall(DATE_PATTERN_1, sentence.text)
    if date_match:
        return date_match
    date_match = re.findall(DATE_PATTERN_3, sentence.text)
    if date_match:
        return date_match
    date_match = re.findall(DATE_PATTERN_2, sentence.text)
    if date_match:
        return date_match
    return "Ohne Datum"



class NeuralRelEx():
    def __init__(self, sentence, pers):
        self.sentence = sentence
        self.pers: classes.Person = pers

    def neural_relation_extraction(self):
        relations = self.sentence.get_relations('relation')
        subjekt = self.pers
        for relation in relations:
            print(f'first: {relation.first.text}, tag{relation.first.tag} second: {relation.second.text}, label: {relation.tag}')
            if relation.first.text in self.pers.get_shorts():
                objekt = entity_linking.get_class(relation.second.text, relation.second.tag)
            elif relation.second.text in self.pers.get_shorts():
                objekt = entity_linking.get_class(relation.first.text, relation.first.tag)
            else:
                subjekt = entity_linking.get_class(relation.first.text, relation.first.tag)
                objekt = entity_linking.get_class(relation.second.text, relation.first.tag)
            rel = classes.Relation(
                        ent_1=subjekt,
                        ent_2=objekt,
                        source=self.sentence.text,
                        rel_type=relation.tag,
                        date=find_date(self.sentence)
                        )
            entity_linking.relationship_collection[self.pers.name].append(rel)


