#!/usr/bin/env python
# -*- coding: utf8 -*-
# @Author: Sandra Bernstein, Richard Aude, Jonas Richter

import regex as re
import i_o

class Preprocess:

    def __init__(self, fileName):
        self.fullText = i_o.load_json(fileName)  # attempt2 laden
        self.person_preprocess()

        i_o.write_json(self.fullText,"personen_clean_neu")

    def person_preprocess(self):
        
        # bio jeder person cleanen und ersetzen
        
        for pers in self.fullText:
            self.person = pers['Beschreibung']
            self.remove_punct_spaces()
            self.dates()
            #self.romanNumerals()
            self.satzendzeichen()
            pers['Beschreibung'] = self.person

    def remove_punct_spaces(self):
        """Erledigt große Teile der Vorverarbeitung. Bestimmte Pattern werden ausgetauscht, Rechtschreibfehler,
        korrigiert, entfernt Anführungszeichen, Leerzeichen bei Bindestrichen und Punkten in einem Wort usw.
"""

        test = re.findall(r'dj\.1\.11', self.person)
        self.person = self.person.replace('\'', '').replace('„', '')
        self.person = re.sub(r'“\s*((?:[-]|$))\s*', r'\1', self.person)
        self.person = re.sub(r'”\s*((?:[-]|$))\s*', r'\1', self.person)
        self.person = re.sub(r'“\s*((?:[.]|$))', r'\1', self.person)
        self.person = re.sub(r'”\s*((?:[.]|$))', r'\1', self.person)
        self.person = re.sub(r'((und\s+|$))-', r'\1', self.person)
        self.person = re.sub(r'\s+((-\p{L}|$))', r'\1', self.person)
        # Lösche Leerzeichen nach Punkten
        test = re.findall(r'(\s\d{1,3}\.)\s', self.person)
        if test:
            test
        self.person = re.sub(r'((?<=\s\d{1,3}\.))\s(?!(Er|Unverhofft|Dort|Zur|Ein|[a-z]))', r'\1', self.person)
        #Ersetze ; und : durch ,
        self.person = re.sub(r';|:', r',', self.person)
        #test = re.findall(r'[^.,!?\s\w],', self.person)
        #if len(test) > 1:
        #    test
        # Lösche Leerzeichen zwischen St. und Wort
        self.person = re.sub(r'(?<=St\.)\s(?=\w)', r'', self.person)
        # Lösche Leerzeichen zwischen Paragraf und Zahl
        self.person = re.sub(r'(?<=§)\s(?=\d)', r'', self.person)
        # Lösche Webseiten
        webpattern = re.compile(r'(www.\S*.\w*)', re.IGNORECASE)
        self.person = re.sub(webpattern, r'.', self.person)
        # Lösche Bildverweise
        bildpattern = re.compile(r"(\([\w\s]*(?:Bild|hier|Foto)\s*[\w\s-ÜÄÖ]*\))")
        self.person = re.sub(bildpattern, r'', self.person)
        # Füge fehlendes Leerzeichen ein
        self.person = re.sub(r'(?<=ermordet)\.(?=Ausführliche)', r'. ', self.person)
        # Entferne Anführungszeichen
        self.person = self.person.replace('“', '').replace('”', '').replace('\"', '')
        # Hänge Straßennummern und Regiemntszahlen an das vorhergehende zugehörige Wort
        #self.person = re.sub(r'(?<=([sS]tra[ssß]e|[bB]attailion|[Rr]egiment|[Ee]inheit[en]*))\s(?=[0-9]{1,3}\s)', r'_', self.person)
        # Lösche Leerzeichen um = und &
        self.person = re.sub(r'\s=\s', r'=', self.person)
        self.person = re.sub(r'\s&\s', r'&', self.person)

        # test = re.findall(r'(?<=([sS]tra[ssß]e|[bB]attailion|[Rr]egiment|[Ee]inheit[en]*))(\s[0-9]{1,3}\s)', self.person)
        if len(test) > 0:
                test
        # Ersetze Gedankenstriche mit ,
        self.person = re.sub(r'\s[^.,!?\s\w&=],*(?=\s)', r',', self.person)
        # Suche Akürzung der deutschen Jungenschaft und mache sie erkennbar
        self.person = re.sub(r'dj.1.11', r'DJ_1.11', self.person)
        # Schreibfehler Korrektur
        self.person = re.sub(r'jungenschaft', r'Jungenschaft', self.person)
        # FIX Seltsamer Fehler während der Textverarbeitung
        self.person = re.sub(r'Nizza.', r'Nizza .', self.person)

    def dates(self):
        """Sucht verschiedene Datums-Pattern"""
        # 1. Juli 1920; 1. Juli
        DATE_PATTERN_1 = re.compile(r'\d{1,2}\.?\s*(?:Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)\w*(?:\s*\d{4}|\d{2})?', re.IGNORECASE)
        # 1920; 1920/21
        DATE_PATTERN_2 = re.compile(r'1\d{3}(?:/1\d{1,3}|/\d{2})?')
        # Juli 1920; Juli 20
        DATE_PATTERN_3 = re.compile(r'(?:Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)\s+(?:1\d{3}|\d{2})(?:/1\d{1,3}|/\d{2})?', re.IGNORECASE)
    
        # gefundene Datumsangaben mit Tag versehen
        dates = re.findall(DATE_PATTERN_2, self.person)
        dates += re.findall(DATE_PATTERN_1, self.person)
        dates += re.findall(DATE_PATTERN_3, self.person)
        dates

    def romanNumerals(self):
        """Entferne Leerzeichen zwischen Worten und römischen Zahlen
        z.B. vii. Wehrkreis -> vii.Wehrkreis"""

        test = re.findall(r'(?<=Wehrkreis[eskommandos]*)\s[MDCLXVI]+\.*', self.person)
        if len(test) > 1:
            test
        #self.person = re.sub(r'(?<=Wehrkreis[eskommandos]*)\s((?=[MDCLXVI]+\b\.*)(?!Salzburg|ein|Böhmen-Mähren))', r'_', self.person)
        self.person = re.sub(r'((?<=(?<!Wilhelm)\s[MDCLXVI]+\b\.*))\s((?=[A-Z]))', r'\1', self.person)
        #self.person = re.sub(r'(?<=Sondergericht|Krematorium|Wilhelm|Pius|Abwehr)\s((?=[MDCLXVI]+\b\.*))', r'_', self.person)
        #self.person = re.sub(r'(?<=und)\s(?=[MDCLXVI]+\b\.*\s)', r' Wehrkreis_', self.person)
        #self.person = re.sub(r'Wehrkreisen_II', r'Wehrkreis_II', self.person)


    def satzendzeichen(self):
        """Füge Satzzeichen am Ende jedes Textes ein, der kein Satzendzeichen hat"""
        test1 = self.person.endswith('.')
        test = re.findall(r'\w[!?]*s*$', self.person)
        self.person = re.sub(r'(?<=\w)\s*$', r'.', self.person)
        if not test1:
            test
        if len(test) > 0:
            test1



#Preprocess("attempt2.json")
