import base64
import networkx as nx
import i_o

from flask import request
from flask_restful import Resource
import classes


class NetworkGraph(Resource):
    """
    The following class is used to handle API-Requests and create fitting API-Responses
    """

    def get(self, node):
        """
        Get Ego-Graph of requested node

        :param node: the node
        :returns
        """
        print(f'get request {node}')
        if node == "DNDW" or node is None:
            return request_handler.graph_builder.generate_serializable_graph()
        elif node:
            request_handler.create_subgraph(node, filtr=[], depth=1, serializable=True)
            return request_handler.graph_builder.generate_serializable_graph(True)

    def post(self):
        if request.is_json:
            request_json = request.get_json()
        else:
            return 'Content type is not supported, please use JSON-format!'
        graph = request_handler.graph_builder.deserialize_graph(request_json)
        i_o.write_pkl(graph, 'playground_graph')
        return (f'post request graph successful')

    def put(self):
        if request.is_json:
            mapping = request.get_json()
        else:
            return 'Content type is not supported, please use JSON-format!'
        print(f'put request with new mapping')
        graph: nx.Graph = i_o.read_pkl('playground_graph')
        nx.relabel_nodes(graph, mapping)
        i_o.write_pkl(graph, 'playground_graph')


class NetworkNode(Resource):
    def get(self, node):
        print(f'get request {node}')
        try:
            return request_handler.graph_builder.get_serialized_node(node)
        except KeyError:
            return 'Fehler! Knoten wurde nicht gefunden', 404

    def post(self):
        if request.is_json:
            node = request.get_json()
            print(f'post request {node["name"]}')
        else:
            return 'Content type is not supported, please use JSON-format!'
        graph: nx.Graph = i_o.read_pkl('playground_graph')
        new_node = request_handler.graph_builder.deserialize_node(node)
        if isinstance(new_node, classes.Person):
            graph.add_node(new_node, tag="PER", color="green")
        elif isinstance(new_node, classes.Location):
            graph.add_node(new_node, tag="LOC", color="blue")
        elif isinstance(new_node, classes.Organisation):
            graph.add_node(new_node, tag="ORG", color="red")
        i_o.write_pkl(graph, 'playground_graph')
        return f'post request of node {node["name"]} successful'

    def put(self, node):
        print(f'put request {node}')
        if request.is_json:
            new_node = request.get_json()
        else:
            return 'Need correctly formatted JSON to work!', 400
        graph: nx.Graph = i_o.read_pkl('playground_graph')
        try:
            graph = request_handler.graph_builder.exchange_node(node, new_node, graph)
        except KeyError:
            return 'Need correctly formatted JSON to work!', 400
        i_o.write_pkl(graph, 'playground_graph')

    def delete(self, node):
        print(f'delete request {node}')
        graph: nx.Graph = i_o.read_pkl('playground_graph')
        try:
            graph = request_handler.graph_builder.delete_node(node, graph)
        except KeyError:
            return 'Nothing to delete! Given node does not exist in this graph.', 200
        i_o.write_pkl(graph, 'playground_graph')
        return f'Node {node} successfully deleted.'


class NetworkEdge(Resource):
    def post(self, node, neighbor):
        print(f'post request {node} edge to {neighbor}')
        graph: nx.Graph = i_o.read_pkl('playground_graph')
        graph.add_edge(node, neighbor)
        i_o.write_pkl(graph, 'playground_graph')

    def delete(self, node, neighbor):
        print(f'delete request {node} edge to {neighbor}')
        graph: nx.Graph = i_o.read_pkl('playground_graph')
        graph.remove_edge(node, neighbor)
        i_o.write_pkl(graph, 'playground_graph')


class NetworkImage(Resource):
    def post(self):
        if request.is_json:
            request_json = request.get_json()
        else:
            return 'Content type is not supported, please use JSON-format!'
        title = "DNDW"
        subgraph = False
        if request_json['node'] and request_json['node'] != "DNDW":
            """Codeblock to generate Graph and Subgraph"""
            request_handler.create_subgraph(node=request_json['node'], filtr=request_json['filtr'],
                                            depth=request_json['depth'])

            title = request_json['node']
            if request_json['node'] is list:
                node = request_json['node'].split(', ')
                if len(node) < 2:
                    node = request_json['node'].split(',')
                title = ", ".join(node)
                title = 'Pfad ' + title

            """latest search is needed to use the search depth"""
            subgraph = True

        if request_json['datatype'] == 'image':
            b64_string = (request_handler.make_plotly(title, subgraph).to_image())
            b64_string = str(base64.b64encode(b64_string))
            b64_string.replace("'", '"')

            return {'b64': b64_string[2:-1]}
        if request_json['datatype'] == 'html':
            return request_handler.make_plotly(title, subgraph).to_html()
        if request_json['datatype'] == 'json':
            return request_handler.make_plotly(title, subgraph).to_plotly_json()
