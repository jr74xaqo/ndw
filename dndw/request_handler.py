
import networkx as nx
import plotly.graph_objects as go

import graph_builder
import i_o


class App:
    def __init__(self, graph_file_name):
        self.myDict = i_o.load_json('attempt9')
        self.graph_builder = graph_builder.GraphBuilder(graph_file_name)

    def create_subgraph(self, node, filtr, depth, serializable=False):
        self.graph_builder.print_subgraph(node_name=node, filtr=filtr, depth=depth)
        if serializable:
            return self.graph_builder.generate_serializable_graph(True)

    def getContext(self, node):
        """
    Falls node dem Namen einer Person aus dem Verzeichnis der Gedenkstätte entspricht, findet diese Methode die jeweiligen Daten (Lebensdaten und Originaltext).
    :param node: Der Knoten für den der Kontext bestimmt werden soll.
    :returns: Eine Liste von Strings, dies sind Name, Lebensdaten und biografischer Text der Person. Falls der Knoten nicht dem Namen einer Person entspricht, wird "not provided" zurück gegeben.
    :rtype: list or String
    """
        context = "not provided"
        for person in self.myDict:
            if person["Name"] == node:
                context = [person["Name"], person["Lebensdaten"], person["Beschreibung"]]
        return context

    def get_edge_trace(self, graph) -> tuple[go.Scatter, go.Scatter]:
        """
    Erstellt für die Darstellung des Graphen erforderliche Kanten mit Labels.

    :returns: Die Linien und Liniengewichte (Labels)
    """
        edge_x = []
        edge_y = []
        xtext = []
        ytext = []
        for edge in graph.edges():
            x0, y0 = graph.nodes[edge[0]]['pos']
            x1, y1 = graph.nodes[edge[1]]['pos']
            xtext.append((x0 + x1) / 2)
            ytext.append((y0 + y1) / 2)
            edge_x.append(x0)
            edge_x.append(x1)
            edge_x.append(None)
            edge_y.append(y0)
            edge_y.append(y1)
            edge_y.append(None)

        edge_trace = go.Scatter(
            x=edge_x, y=edge_y,
            line=dict(width=0.5, color='#888'),
            mode='lines'
        )

        eweights_trace = go.Scatter(x=xtext, y=ytext, mode='text',
                                    marker_size=0.5,
                                    text=self.format_edge_list(nx.get_edge_attributes(graph, "type")),
                                    customdata=[[dt.org_date for dt in t if t] for t in [dates for dates in
                                                                                         self.format_edge_list(
                                                                                             nx.get_edge_attributes(
                                                                                                 graph, "date"))]],
                                    hovertext=self.format_edge_list(
                                        nx.get_edge_attributes(graph, "source")),
                                    textposition='top center',
                                    hovertemplate='%{text}<br>%{customdata}<extra></extra>')
        return edge_trace, eweights_trace

    def format_edge_list(self, relations) -> list:
        """
    Gibt Liste von Relationsnamen für Mapping zurück.

    :param: relations: Dictionary, Attributliste von Networkx-Graph.
    :returns: Liste von Relationsnamen für Mapping auf Knoten des Graphen.
    """
        relationsList = []
        for relation in relations:
            relationsList.append(relations[relation])
        return relationsList

    def get_node_trace(self, nodes) -> go.Scatter:
        """
        Erstellt für die Darstellung des Graphen erforderliche Knoten mit Labels.

        :returns: Die Knoten mit Labels und Hoverinformationen
        :rtype: plotly.graph_objs._scatter.Scatter Objekt
        """

        node_x = []
        node_y = []
        node_text = []
        node_color = []
        node_tags = []
        for node in nodes:
            node_text.append(node.name)
            x, y = nodes[node]['pos']
            node_x.append(x)
            node_y.append(y)
            node_color.append(nodes[node]["color"])
            node_tags.append(nodes[node]["tag"])

        node_trace = go.Scatter(
            x=node_x, y=node_y,
            mode='markers+text',
            text=node_text,
            customdata=node_tags,
            textposition="top center",
            marker_size=12,
            hoverinfo="text",
            hovertemplate='%{text}<br>Tag: %{customdata}<br>%{x},%{y} <extra></extra>',
            marker_color=node_color
        )

        return node_trace

    def make_plotly(self, title, subgraph=False) -> go.Figure:
        """
        Erstellt Html- und Javascript-Quelltext für die Darstellung des Graphen

        :param: G: Networkx-Graph
        :param: title: Titel, welcher dem Plot (der Darstellung des Graphens) gegeben werden soll.
        :returns: Html und Javascript zum Einbetten
        :rtype: String
        """

        # Koordinaten einzelner Knoten werden nach dem Fruchterman-Rheingold Algorithmus berechnet.
        pos_dict = self.graph_builder.pos_dict
        fixed_node = self.graph_builder.fixed_node
        weight = nx.get_edge_attributes(self.graph_builder.subgraph, 'date_weight')
        g = self.graph_builder.graph
        if subgraph:
            g = self.graph_builder.subgraph

        pos = nx.spring_layout(g, weight='date_weight', pos=self.graph_builder.pos_dict,
                               fixed=self.graph_builder.fixed_node, center=(0, 0))
        #pos = nx.spectral_layout(g, weight='date_weight', center=(0, 0))
        nx.set_node_attributes(g, pos, 'pos')
        edge_trace, eweights_trace = self.get_edge_trace(g)
        node_trace = self.get_node_trace(g.nodes())
        fig = go.Figure(data=[edge_trace, node_trace, eweights_trace],
                        layout=go.Layout(
                            title='<br>' + title + '<br>',
                            titlefont_size=16,
                            showlegend=False,
                            hovermode='closest',
                            margin=dict(b=20, l=5, r=5, t=30),
                            xaxis_visible=False,
                            yaxis_visible=False,
                            autosize=True,
                            #width=600,
                            #height=600

                        )
                        )
        fig.update_layout(clickmode='event')
        # div = plotly.offline.plot(fig, include_plotlyjs=False, output_type='div')

        return fig
