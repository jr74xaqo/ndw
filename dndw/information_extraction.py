import logging

from flair.splitter import SegtokSentenceSplitter
import entity_linking
import i_o
from relation_extraction import RelationExtraction, NeuralRelEx
import classes

logger = logging.getLogger("dndw")




def extract_information(filename, testing, unwikificated, no_coref=True, neural_rel_ex = True, debug_data=None):
    splitter = SegtokSentenceSplitter()
    # load file
    logger.info("Starting information extraction")
    if debug_data:
        data = debug_data
    else:
        data = i_o.load_json("personen_clean")
        if testing:
            data = data[0:1]
            print(data)
        #data = data[10:11]
    # load model
    models = i_o.load_models(no_coref, neural_rel_ex)

    #splitter = SpacySentenceSplitter()
    try:
        collections = i_o.read_pkl(filename)
        entity_linking.relationship_collection = collections[0]
        entity_linking.entity_collection = collections[1]
    except (KeyError, EOFError):
        logger.info(f"Collection with name {filename} does not exist a new collection will be generated")
        # keep the following two lines for quick runs on 'Wolfgang Abendroth'

    for person in data:
        named_entities = []
        org_text = person["Beschreibung"]
        pers = classes.Person(person['Name'], person['Lebensdaten'])
        if pers.name in list(entity_linking.relationship_collection.keys()) and not testing:
            continue
        entity_linking.entity_collection[pers.name] = pers
        entity_linking.entity_set.add(pers)
        # for sent in tagged_sents:
        #    if pers.name not in sent:
        #        re.sub(r'\b(er|sie|ihm|ihr)\b', pers.name, sent, count=1)
        tagged_sents = splitter.split(org_text)
        for model in models:
            model.predict(tagged_sents)
        entity_linking.relationship_collection[pers.name] = []

        for sentence in tagged_sents:
            if sentence and not neural_rel_ex:
                relation = RelationExtraction(sentence, pers, unwikificated).relation_extraction()
                named_entities.append(pers)
            elif sentence and neural_rel_ex:
                NeuralRelEx(sentence, pers).neural_relation_extraction()
        logger.info(f"{pers.name} processed")
        t = entity_linking.relationship_collection, entity_linking.entity_collection

        i_o.write_pkl([entity_linking.relationship_collection, entity_linking.entity_collection], filename)

data = [{'Name': 'Wolfgang Abendroth', 'Lebensdaten': '02. Mai 1906 - 15. September 1985', 'Beschreibung': 'Wolfgang Abendroth wird 1906 in Elberfeld geboren und wächst in einer sozialdemokratischen Lehrerfamilie auf. Bereits als 14jähriger tritt er in die Kommunistische Jugend Deutschlands ein. Aus der KPD wird er 1928 wegen seines Eintretens für eine Einheitsfront von Sozialdemokraten und Kommunisten ausgeschlossen. 1933 wird er als Rechtsreferendar aus politischen Gründen entlassen und berät seitdem viele Regimegegner juristisch. Nach seiner ersten Verhaftung emigriert Abendroth in die Schweiz, wo er auch promoviert. Nach längerer Kuriertätigkeit entschließt er sich 1935 zur Rückkehr nach Berlin. Hier betätigt er sich aktiv bis zu seiner mehrjährigen Inhaftierung 1937 im Widerstand. Seit Februar 1943 Soldat einer der Bewährungseinheiten_999 desertiert er im September 1944 zur griechischen Partisanenorganisation ELAS. Nach seiner Gefangennahme durch die Engländer beteiligt er sich an der politischen Aufklärungsarbeit von Regimegegnern in Kriegsgefangenschaft.', 'Literatur': ['Wolfgang Abendroth: Der gemeinsame Kampf mit den Griechen. In: Information der Interessengemeinschaft ehemaliger deutscher Widerstandskämpfer in den vom Faschismus okkupierten Ländern. 1978, Nr. 3, S. 11-12', 'Wolfgang Abendroth: Ein Leben in der Arbeiterbewegung. Gespräche, aufgezeichnet und herausgegeben von Barbara Dietrich und Joachim Perels. Frankfurt am Main 1977', 'Andreas Diers: Arbeiterbewegung - Demokratie - Staat. Wolfgang Abendroth. Leben und Werk 1906-1948. Hamburg 2006']}]
#extract_information(filename='', unwikificated=False, testing=False, debug_data=data)
