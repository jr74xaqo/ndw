from collections import Counter
from datetime import date, datetime
from itertools import chain

import networkx as nx
from more_itertools import powerset

import classes
import i_o
import locale
import math

locale.setlocale(locale.LC_ALL, ('de_DE', 'UTF-8'))


def get_tags():
    tags = []
    s = list(["PER", "LOC", "ORG", "DATE"])
    powset = powerset(s)
    powset.__next__()
    for tag in powset:
        tags.append(set(tag))
    return tags


class GraphBuilder:
    def __init__(self, filename: str = None, graph: nx.Graph = None):
        self.graph = nx.Graph()
        if filename:
            self.graph = i_o.read_pkl(filename)
            if type(self.graph) is dict:
                graph_json = i_o.read_pkl(filename)
                self.graph = nx.node_link_graph(graph_json)
        self.tags = get_tags()
        if graph:
            self.graph = graph
        self.names = [node.name for node in self.graph.nodes]
        self.name_dict = {node.name: node for node in self.graph.nodes}
        self.short_dict = {node.name: node.shorts for node in self.graph.nodes}
        self.subgraph = nx.Graph()
        self.pos_dict = None
        self.fixed_node = None
        self.serialized_graph = self.generate_serializable_graph()

    def get_graph(self):
        return self.graph

    def build_graph(self, filename: str):
        collections = i_o.read_pkl(filename)
        collection = collections[0]
        for person in collection:
            for relation in collection[person]:
                if relation.ent1 and relation.ent2:
                    self.node_adder(relation.ent1)
                    self.node_adder(relation.ent2)
                    if not relation.type:
                        types = f"{relation.ent1.name}+{relation.ent2.name}"
                        i_o.write_repl_valid(relation.source, types , filename+"empty_sents")
                    if relation.ent2 not in list(self.graph[relation.ent1]):
                        if relation.is_event:
                            self.graph.add_edge(u_of_edge=relation.ent1, v_of_edge=relation.ent2, date=relation.date,
                                                source=relation.source, is_event=relation.is_event, type=relation.type,
                                                date_weight = 1)
                        else:
                            self.graph.add_edge(u_of_edge=relation.ent1, v_of_edge=relation.ent2, date=relation.date,
                                                source=relation.source, is_event=relation.is_event, type=relation.type,
                                                date_weight = 1)
                    else:
                        self.graph[relation.ent1][relation.ent2]['source'] += "\n" + relation.source
                        self.graph[relation.ent1][relation.ent2]['type'] += relation.type
        i_o.write_pkl(self.graph, f'{filename}_graph' )


    def node_adder(self, entity):
        if isinstance(entity, classes.Person):
            self.graph.add_node(entity, tag="PER", color="green")
        elif isinstance(entity, classes.Location):
            self.graph.add_node(entity, tag="LOC", color="blue")
        elif isinstance(entity, classes.Organisation):
            self.graph.add_node(entity, tag="ORG", color="red")

    def print_subgraph(self, node_name="", node2_name="", filtr=[], depth=1, single_graph=False, relation:str=None):
        """Gibt einen Teilgraphen des in construct_graph() erstellten Graphen abhängig von der Eingabe
        wieder.

        :param node: Ein Knoten im Graph, der im Fokus der Anfrage steht.
        :parameter node2: Ein weiterer Knoten, der Teil der Anfrage sein soll.
        :param filtr: ermöglicht das Filtern der Knoten anhand der Tags: LOC, PER, ORG, DATE, MISC
        :param depth: ermöglicht in Kombination mit node die Suchtiefe einzustellen, default ist 1.
        :param single_graph: Boolean, regelt Ausgabe eines Graphen für EINE bestimmte Person ermöglicht default ist False.

        :returns subgraph: Teilgraph des in construct_graph() erstellten Graphen.
        Abhängig von der Kombination der Eingaben werden verschiedene Teilgraphen erzeugt.

        print_graph(node) gibt den Knoten und *alle* Nachbarn, aus.
        *Handelt es sich bei node um eine PERson wird auch der jeweilige Graph UNABHÄNGIG von depth ausgegeben*.


        print_graph(node, node2) gibt alle kürzesten Wege zwischen node und node2 aus.

        print_graph(node, filtr) gibt node und die Nachbarn von node aus, die den Tag von filtr haben aus
        *Handelt es sich bei node um eine PERson werden alle Knoten des zugehörigen Graphen ausgegeben, die dem filtr
        entsprechend getaggt sind. Unabhängig von depth*

        print_graph(node, node2, filtr) die alle kürzesten Wege zwischen node und node2 aus, die mit filtr getaggt sind

        print_graph(filtr) gibt alle Knoten des Ausgangsgraphen aus, die mit filtr getaggt sind.

        print_graph() gibt den gesamten Graphen aus.
        """
        node, node2 = None, None
        if isinstance(node_name, str):
            if node_name:
                try:
                    node = self.name_dict[node_name]
                except KeyError:
                    answ_set = [val for val in list(self.short_dict.values()) if node_name in val][0]
                    node_name = list(self.short_dict.keys())[list(self.short_dict.values()).index(answ_set)]
                    node = self.name_dict[node_name]
        if isinstance(node_name, list):
            try:
                node = [self.name_dict[nd] for nd in node_name]
            except KeyError:
                nd_list = []
                for nd in node_name:
                    answ_set = [val for val in list(self.short_dict.values()) if nd in val][0]
                    nd_list.append(list(self.short_dict.keys())[list(self.short_dict.values()).index(answ_set)])
                    node = [self.name_dict[nd] for nd in nd_list]
            node2 = True
        subnodes = []

        event_filter = False
        if filtr and 'DATE' in filtr:
            event_filter = True
            if len(filtr) == 1:
                filtr.extend(['PER', 'LOC', 'ORG'])

        empty_filter = False
        if filtr and 'EMPTY' in filtr:
            empty_filter = True
            if len(filtr) == 1:
                filtr.extend(['PER', 'LOC', 'ORG'])

        if filtr == ['PER', 'LOC', 'ORG'] or not filtr:
            filtr = []


        tag = nx.get_node_attributes(self.graph, "tag")
        # filtr = filtr.replace(" ", "")
        # filtr = filtr.split(",")

        #if filtr in self.tags and node and node2:
        #    subnodes.extend(node)
        #    for nd in node:
        #        tmp = node
        #        tmp.pop(node.index(nd))
        #        for nd2 in tmp:
        #            if nx.has_path(self.graph, nd, nd2):
        #                subnodes.extend(nx.shortest_path(self.graph, node, node2))
        if empty_filter:
            for node in list(self.graph.nodes):
                for neighbor in list(self.graph[node]):
                    if not self.graph.edges[node, neighbor]['type']:
                        test = self.graph.edges[node, neighbor]['type']
                        subnodes.append(node)
                        subnodes.append(neighbor)
        if relation and node and filtr:
            subnodes.append(node)
            for neighbor in list(self.graph[node]):
                if tag[neighbor] in filtr and neighbor not in subnodes:
                    if event_filter and self.graph.edges[node, neighbor]['is_event'] and relation in self.graph.edges[node, neighbor]['type']:
                        subnodes.append(neighbor)
                    elif not event_filter and relation in self.graph.edges[node, neighbor]['type']:
                        subnodes.append(neighbor)

        elif relation and node:
            subnodes.append(node)
            for neighbor in list(self.graph[node]):
                if relation in self.graph.edges[node, neighbor]['type']:
                    subnodes.append(neighbor)

        elif relation:
            for nd, neigh, rel_type in self.graph.edges.data('type'):
                if relation in rel_type:
                    subnodes.extend([nd, neigh])


        elif set(filtr) in self.tags and node:
            # for pers in data:
            #    if pers['Name'] == node:
            #        graph.add_node(node, color="green", tag="PER")
            #        relation = pers['Relations']
            #        extract_relation(graph, relation, node)

            if depth >= 0:
                subnodes.append(node)
            if depth >= 1:
                for neighbor in list(self.graph[node]):
                    if tag[neighbor] in filtr and neighbor not in subnodes:
                        if event_filter and self.graph.edges[node, neighbor]['is_event']:
                            subnodes.append(neighbor)
                        elif not event_filter:
                            subnodes.append(neighbor)
            if depth >= 2:
                for neighbor in list(self.graph[node]):
                    if tag[neighbor] in filtr and neighbor not in subnodes:
                        for n in list(self.graph[neighbor]):
                            if tag[n] in filtr and n not in subnodes:
                                if event_filter and self.graph.edges[n, neighbor]['is_event']:
                                    subnodes.append(n)
                                elif not event_filter:
                                    subnodes.append(n)
            if depth >= 3:
                for neighbor in list(self.graph[node]):
                    if tag[neighbor] in filtr and neighbor not in subnodes:
                        for n in list(self.graph[neighbor]):
                            if tag[n] in filtr and n not in subnodes:
                                for i in list(self.graph[n]):
                                    if tag[i] in filtr and i not in subnodes:
                                        if event_filter and self.graph.edges[n, i]['is_event']:
                                            subnodes.append(i)
                                        elif not event_filter:
                                            subnodes.append(i)
            if depth >= 4:
                for neighbor in list(self.graph[node]):
                    if tag[neighbor] in filtr and neighbor not in subnodes:
                        for n in list(self.graph[neighbor]):
                            if tag[n] in filtr and n not in subnodes:
                                for i in list(self.graph[n]):
                                    if tag[i] in filtr and i not in subnodes:
                                        for j in list(self.graph[i]):
                                            if tag[j] in filtr and j not in subnodes:
                                                if event_filter and self.graph.edges[j, i]['is_event']:
                                                    subnodes.append(j)
                                                elif not event_filter:
                                                    subnodes.append(j)

        elif node2 and node:
            subnodes.extend(node)
            for nd in node:
                node.pop(node.index(nd))
                for nd2 in node:
                    node.pop(node.index(nd2))
                    if nx.has_path(self.graph, nd, nd2):
                        subnodes.extend(nx.shortest_path(self.graph, nd, nd2))

        elif set(filtr) in self.tags:
            tag = nx.get_node_attributes(self.graph, "tag")
            for n in nx.nodes(self.graph):
                if tag[n] in filtr:
                    subnodes.append(n)

        elif single_graph and tag[node] == 'PER':
            # nx.write_graphml(graph, "" + fname + ".graphml")
            return self.graph

        elif node:
            # for pers in data:
            #     if pers['Name'] == node:
            #         relation = pers['Relations']
            #         extract_relation(self.graph, relation, node)
            #subnodes.extend(nx.nodes(self.graph))
            if depth >= 0:
                subnodes.append(node)
            if depth >= 1:
                subnodes.extend(list(self.graph[node]))
            if depth >= 2:
                for neighbor in list(self.graph[node]):
                    subnodes.extend(list(self.graph[neighbor]))
            if depth >= 3:
                for neighbor in list(self.graph[node]):
                    for n in list(self.graph[neighbor]):
                        subnodes.extend(list(self.graph[n]))
            if depth >= 4:
                for neighbor in list(self.graph[node]):
                    for n in list(self.graph[neighbor]):
                        for i in list(self.graph[n]):
                            subnodes.extend(list(self.graph[i]))

        elif node2_name and not node_name:
            self.print_subgraph(node=node2_name, filtr=filtr, depth=depth, single_graph=single_graph)

        elif not node and not node2 and not filtr:
            subnodes = nx.nodes(self.graph)
        else:
            subnodes = nx.nodes(self.graph)

        self.subgraph = self.graph.subgraph(subnodes)
        self.weights_in_subgraph(node)
        self.set_node_pos(node)

        # nx.write_graphml(subgraph, fname + ".graphml")
        return self.subgraph

    def set_node_pos(self, initial_node):
        self.pos_dict = {initial_node: (0, 0)}
        self.fixed_node = [initial_node]

        adj_nodes = self.subgraph[initial_node]
        event_nodes = [adj for adj in adj_nodes if self.subgraph.edges[initial_node, adj]['is_event']]
        non_event_nodes = [adj for adj in adj_nodes if not self.subgraph.edges[initial_node, adj]['is_event']]

        try:
            initial_phi_en = (math.pi*1/2)/ len(event_nodes)
        except ZeroDivisionError:
            initial_phi_en = 0
        phi_event_nodes = math.pi*3/4

        try:
            initial_phi_nen = (math.pi*1/2)/len(non_event_nodes)
        except ZeroDivisionError:
            initial_phi_nen = 0
        phi_non_event_nodes = math.pi*7/4

        for adj in adj_nodes:
            if adj in event_nodes:
                self.apply_edge_weights(phi_event_nodes, initial_node, adj)
                phi_event_nodes += initial_phi_en
            else:
                self.apply_edge_weights(phi_non_event_nodes, initial_node, adj)
                phi_non_event_nodes += initial_phi_nen

        #nx.spectral_layout(nx.subgraph(self.graph, event_nodes), 'date_weight')

    def apply_edge_weights(self, phi, initial_node, adj):
        normal_postion = (math.cos(phi), math.sin(phi))
        edge_weight = self.subgraph.edges[initial_node, adj]['date_weight']
        weighted_position = tuple(i * edge_weight for i in normal_postion)
        self.fixed_node.append(adj)
        self.pos_dict[adj] = weighted_position

    def weights_in_subgraph(self, initial_node):
        """
        Calculates edge weights for subgraphs.
        Using the difference between 100 years after the end of WWII in europe and the date an event happened.
        If no date is found
        """
        comp_value = date(1995, 5, 8)
        for edge in self.subgraph.edges(initial_node, data=True):
            edge_data = edge[2]
            if not(initial_node is edge[1] or initial_node is edge[0]):
                edge_data['date_weight'] = 0
            elif edge_data['is_event']:
                event_date = self.datestring_to_date(edge_data['date'][-1])

                time_diff = comp_value - event_date
                date_weight = time_diff.days
                edge_data['date_weight'] = date_weight/1000
            else:
                time_diff = comp_value - date(1945, 5, 8)
                edge_data['date_weight'] = time_diff.days/2000

    def datestring_to_date(self, date_obj):
        day = "01"
        month = "Januar"
        year = "0001"
        if date_obj.day:
            day = int(date_obj.day)
        if date_obj.month:
            month = date_obj.month
        if date_obj.year:
            year = int(date_obj.year)

        date_string = f'{year} {month} {day}'
        return datetime.strptime(date_string, '%Y %B %d').date()


    def graph_stats(self):
        """Berechnet Graphstatistiken und gibt die in der Konsole aus"""
        min_degree = 325
        max_degree = 0
        node_list = {}
        degree_list = []
        for node in nx.nodes(self.graph):
            if max_degree < nx.degree(self.graph, node):
                max_degree = nx.degree(self.graph, node)
            if min_degree > nx.degree(self.graph, node):
                min_degree = nx.degree(self.graph, node)
            node_list[node] = nx.degree(self.graph, node)
            degree_list.append(nx.degree(self.graph, node))
        degree_list = sorted(degree_list, reverse=True)
        degree_list_trim = degree_list[:10]
        node_list = sorted(node_list, key=node_list.get, reverse=True)
        node_list_trim = node_list[:10]
        for i,node in enumerate(node_list_trim, start=1):
            print(f"Rang: {i} \t Grad: {degree_list_trim[i-1]} \t Knoten: {node.name}")
        tag = nx.get_node_attributes(self.graph, "tag")
        for t in ["PER", "LOC", "ORG"]:
            c = 1
            print(f"{t}")
            for i, node in enumerate(node_list, 1):
                if tag[node] == t and c <=10:
                    print(f"Rang: {i} \t Grad: {degree_list[i-1]} \t Knoten: {node.name}")
                    c += 1
                elif c > 10:
                    break
        print(f"Maximalgrad: {max_degree}")
        print(f'Minimalgrad: {min_degree}')
        node_list = []
        for node in nx.nodes(self.graph):
            if nx.degree(self.graph, node) == 1:
                node_list.append(node)
        print("Zahl der Knoten mit Grad 1: " + len(node_list).__str__())
        print(f"Zahl der Kanten: {nx.number_of_edges(self.graph)}")
        print(f"Zahl der Knoten: {nx.number_of_nodes(self.graph)}")
        rel_types = list(chain.from_iterable(nx.get_edge_attributes(self.graph, 'type').values()))
        list_counter = Counter(rel_types)
        print(f"10 häufigste Relationstypen: {list_counter.most_common(10)}")
        #print(f"Gesamtzahl verschiedener Relationstypen: {list_counter.total()}")
        if nx.is_connected(self.graph):
            print('Der Graph ist zusammenhängend')
            print('Berechne Durchmesser. Dies kann einige Minuten dauern.')
            print(f'Durchmesser: {nx.algorithms.diameter(self.graph)}')
        else:
            print('Der Graph ist nicht zusammenhängend')
            print(f'Anzahl der zusammenhängenden Komponenten: {nx.number_connected_components(self.graph)}')
            print(f'Größe der zusammenhängenden Komponenten: {[len(c) for c in sorted(nx.connected_components(self.graph), key=len, reverse=True)]}')

    def print_all_relations(self):
        edge = self.graph.edges
        data = edge.data()
        all_relations = []
        for d in data:
            relation = f"{d[0].name} - {d[2]['type']} - {d[1].name}"
            all_relations.append(relation)

        output = '\n'.join(all_relations)
        with open("../../../../universitaet_gruppen/the_connectionists/model_trainer/data/alle_relationen.txt", 'w', encoding='UTF-8') as f:
            f.writelines(output)

    def generate_serializable_graph(self, subgraph = False):
        g = self.graph
        if subgraph:
            g = self.subgraph
        serializable_graph = {'nodes':[], 'edges': []}
        for node in nx.nodes(g):
            serializable_graph['nodes'].append(self.get_serialized_node(node.name))
            serializable_graph['edges'].append(self.serialize_edge(node.name, subgraph))
            for neighbor in g[node]:
                serializable_graph['nodes'].append(self.get_serialized_node(neighbor.name))

        return serializable_graph

    def get_serialized_node(self, node_name):
        node = self.name_dict[node_name]
        node_dict:dict = node.__dict__
        node_dict['shorts']= list(node_dict['shorts'])
        #ToDo: fix the broken sources filed
        try:
            if node_dict['sources']:
                node_dict.pop('sources')
        except KeyError:
            pass
        if type(node) is classes.Person:
            try:
                node_dict['birthday'] = node_dict['birthday'].org_date
            except AttributeError:
                if type(node_dict['birthday']) is str:
                    pass
            except KeyError:
                pass
            try:
                node_dict['day_of_death'] = node_dict['day_of_death'].org_date
            except AttributeError:
                if type(node_dict['day_of_death']) is str:
                    pass
            except KeyError:
                pass
        node_dict['instance'] = str(type(node)).strip("<classes. ''>")
        return node_dict

    def serialize_edge(self, node_name, subgraph=False):
        g = self.graph
        if subgraph:
            g = self.subgraph
        edges_to_neighbors = g.edges(self.name_dict[node_name], data=True)
        for edge in edges_to_neighbors:
            return {'date': [date.org_date for date in edge[2]['date']], 'source': edge[2]['source'], 'is_event': edge[2]['is_event'], 'type': edge[2]['type'], 'edge': [edge[0].name, edge[1].name]}


    def deserialize_graph(self, json_graph):
        graph = nx.Graph()
        new_nodes = dict()
        for node in json_graph['nodes']:
            new_nodes[node['name']] = self.deserialize_node(node)

        for entity in new_nodes.values():
            self.deserialized_node_adder(graph, entity)
        for edge in json_graph['edges']:
            if type(edge['date']) is list:
                date = [classes.Date(d) for d in edge['date']]
            else:
                date = classes.Date(edge['date'])
            graph.add_edge(new_nodes[edge['edge'][0]], new_nodes[edge['edge'][1]], date=date, source=edge['source'], is_event = edge['is_event'], type=edge['type'], date_weight=1)

        return graph

    def deserialized_node_adder(self, graph, entity):
            if isinstance(entity, classes.Person):
                graph.add_node(entity, tag="PER", color="green")
            elif isinstance(entity, classes.Location):
                graph.add_node(entity, tag="LOC", color="blue")
            elif isinstance(entity, classes.Organisation):
                graph.add_node(entity, tag="ORG", color="red")

    def deserialize_node(self, node):
        if node['instance'] == 'Location':
            return classes.Location(node['name'], node['shorts'], node['wikificated'],
                                                       node['coordinates'])
        if node['instance'] == 'Person':
            return classes.Person(node['name'], node['shorts'], node['wikificated'],
                                                     node['birthday'], node['day_of_death'])
        if node['instance'] == 'Organisation':
            return classes.Organisation(node['name'], node['shorts'], node['wikificated'])

    def exchange_node(self, node, new_node_dict, graph:nx.Graph):
        new_node = self.deserialize_node(new_node_dict)
        name_dict = {node.name: node for node in graph.nodes}
        node_to_change = name_dict[node]
        self.deserialized_node_adder(graph, new_node)
        edges_to_neighbors = graph.edges(name_dict[node], data=True)
        for neighbor in edges_to_neighbors:
            edge = neighbor[2]
            graph.add_edge(new_node, neighbor[1], date=edge['date'], source=edge['source'], is_event = edge['is_event'], type=edge['type'], date_weight=1)

        graph.remove_node(node_to_change)
        return graph

    def delete_node(self, node_name, graph:nx.Graph):
        name_dict = {node.name: node for node in graph.nodes}
        node_to_delete = name_dict[node_name]
        graph.remove_node(node_to_delete)
        return graph
