import json

with open('data/personen_clean.json', 'r', encoding='UTF-8') as file:
    data = json.load(file)
    pers_pers_matrix = {person['Name']: [beschreibung['Name'] for beschreibung in data if person['Name'] in beschreibung['Beschreibung']]
                        for person in data}
    most_references = '', 0
    avg_refernces = 0
    for name in pers_pers_matrix:
        liste = pers_pers_matrix[name]
        if len(liste) > most_references[1]:
            most_references = name,len(liste)
        avg_refernces += len(liste)
    avg_refernces = avg_refernces/len(pers_pers_matrix)
    print(f"Person mit meisten Vorkommen: {most_references}")
    print(f"Durchschnittliches Vorkommen: {avg_refernces}")
