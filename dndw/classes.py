import re
import string
from typing import Union

import numpy as np


class Entity:
    def __init__(self, name: str, gnd_id:str=None, variant_names:list=None):
        if gnd_id:
            self.gnd_id = gnd_id
        else:
            self.gnd_id = ""
        self.name = name
        self.shorts = {name}
        if variant_names:
            self.shorts.update(variant_names)
        else:
            self.shorts.update(name.split())
            self.shorts.update(name.split('_'))
        self.sources = []
        self.wikificated = False

    def get_shorts(self):
        return self.shorts

    def __str__(self):
        return self.name


    def add_shorts(self, shorts: str):
        self.shorts.update(shorts.split())
        self.shorts.update(shorts.split('_'))
        self.shorts.add(shorts)

    def set_sources(self, sources: []):
        self.sources.extend(sources)

    def add_source(self, source: str):
        self.sources.append(source)

    def set_wikificated(self):
        self.wikificated = True


class Person(Entity):
    def __init__(self, name, lebensdaten=None):
        if lebensdaten:
            if not isinstance(lebensdaten, list):
                lebensdaten = lebensdaten.split('-')
            if len(lebensdaten) == 2:
                self.birthday = Date(lebensdaten[0])
                self.day_of_death = Date(lebensdaten[1])
            if len(lebensdaten) == 1:
                self.birthday = Date(lebensdaten[0])
        Entity.__init__(self, name)


    def set_birthday(self, birthday):
        self.birthday = birthday

    def set_day_of_death(self, day_of_death):
        self.day_of_death = day_of_death


class Organisation(Entity):
    def __init__(self, name):
        Entity.__init__(self, name)



class Location(Entity):
    def __init__(self, name):
        Entity.__init__(self, name)
        self.coordinates = ""
        self.iso_code = ''


    def set_coordinates(self, coordinates):
        self.coordinates = coordinates

    def set_iso_code(self, iso_code):
        self.iso_code = iso_code

    def get_coordinates(self):
        return self.coordinates

    def get_iso_code(self):
        return self.iso_code

    def get_iso_code_vector(self):
        parts = self.iso_code.split('-')
        binary_vectors = []
        for part in parts:
            ascii_sum = sum(ord(char) for char in part)
            bin_rep = format(ascii_sum, '09b')
            binary_vector = np.array((int(bit) for bit in bin_rep), dtype=np.uint8)
            binary_vectors.append(binary_vector)
        while len(binary_vectors) < 3:
            binary_vectors.append(np.zeros((9,), dtype=np.uint8))
        assert len(binary_vectors) == 3 and len(binary_vectors[0]) == len(binary_vectors[2])
        return np.concatenate(binary_vectors)


def create_binary_mapping():
    alphabet = string.ascii_uppercase+'-'  # 'A' to 'Z'
    mapping = {}

    for i, letter in enumerate(alphabet):
        # Erstelle eine Binärsequenz mit einer Länge von 5 Bit (genug, um 26 Buchstaben darzustellen)
        bin_sequence = format(i, '05b')  # Wandelt 'i' in eine 5-Bit-Binärzahl um
        mapping[letter] = bin_sequence

    return mapping

# Mapping für Buchstaben
binary_mapping = create_binary_mapping()


class Relation:
    def __init__(self, ent_1: Entity, ent_2: Entity, rel_type: list[str], source: str, date=None):
        self.ent1 = ent_1
        self.ent2 = ent_2
        self.type = rel_type
        self.source = source
        self.is_event = False
        if date == "Ohne Datum":
            self.date = [Date(date)]
        elif date:
            self.date = [Date(dt) for dt in date]
            self.is_event = True


class Date:
    def __init__(self, date: Union[list[str], str]):
        self.year = ""
        self.month = ""
        self.day = ""

        if date in ("Ohne Datum","",['Ohne Datum']):
            self.org_date = [date]
        else:
            self.date_pattern(date)
            self.org_date = date
        self.year_vector = None



    def date_pattern(self, date):
        """Sucht verschiedene Datums-Pattern"""
        # 1. Juli 1920; 1. Juli
        DATE_PATTERN_1 = re.compile(
            r'\d{1,2}\.?\s*(?:Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)\w*(?:\s*\d{4}|\d{2})?',
            re.IGNORECASE)
        # 1920; 1920/21 v525
        # Juli 1920; Juli 20
        DATE_PATTERN_3 = re.compile(
            r'(?:Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)\s+(?:1\d{3}|\d{2})(?:/1\d{1,3}|/\d{2})?',
            re.IGNORECASE)
        #alle Jahre inkl. v.Chr. codiert als v0...v9999
        DATE_PATTERN_4 = re.compile(r'^(?:v[1-9]\d{0,3}|[1-9]\d{0,3}|[1-9]\d{4})')

        # gefundene Datumsangaben mit Tag versehen
        if re.match(DATE_PATTERN_1, date):
            date = re.split(r'\W+', date)
            self.day = date[0]
            self.month = date[1]
            try:
                self.year = date[2]
            except IndexError:
                self.year = ""
        elif re.match(DATE_PATTERN_3, date):
            date = date.split()
            self.month = date[0]
            self.year = date[1]
        # elif re.match(DATE_PATTERN_2, date):
         #   self.year = date
        elif re.match(DATE_PATTERN_4, date):
            self.year = date

    def year_to_binary_vector(self):
        binary_rep = np.binary_repr(int(self.year), width=14)
        binary_vector = np.array(list(binary_rep), dtype=int)
        self.year_vector = binary_vector

    def get_year_vector(self):
        return self.year_vector


