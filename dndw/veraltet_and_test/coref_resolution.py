import json
import subprocess
import platform
from CorZu import conll_to_text


def main(text: str, name: str) -> str:
    file = open("tmp/test.txt", 'w', encoding='utf-8')
    file.write(text)
    file.close()
    if platform.system() == 'Windows':
        subprocess.run(['C:\\Users\\Jonas\\AppData\\Local\\Microsoft\\WindowsApps\\ubuntu2004', './CorZu/corzu.sh', 'tmp/test.txt'])
    else:
        subprocess.run(['./CorZu/corzu.sh', 'tmp/test.txt'])
    result_text = conll_to_text.convert_to_text(name)
    return result_text


def run_alone():
    with open('../data/personen_clean.json', "r", encoding="UTF-8") as read_file:
        write_file = open('../data/rel_ex_ready.json', "r", encoding='UTF-8')
        pers_list = json.load(write_file)
        processed_persons = [pers['Name'] for pers in pers_list]
        write_file.close()
        data = json.load(read_file)
        for person in data:
            if person['Name'] not in processed_persons:
                in_text = person['Name'] + '.\n' +person['Beschreibung']
                name = person['Name']
                coref_text = main(in_text, name)
                person['coref_text'] = coref_text
                save_text = input(f'Ist der Text zufriedenstellend? (y/n)')
                if save_text == 'y':
                    write_file = open('../data/rel_ex_ready.json', "w+", encoding='UTF-8')
                    pers_list.append(person)
                    json.dump(pers_list, write_file, ensure_ascii=False, indent=4)
                    write_file.seek(0)
                    write_file.close()


if __name__ == '__main__':
    run_alone()



#main('Wolfgang Abendroth wird 1906 in Elberfeld geboren und wächst in einer sozialdemokratischen Lehrerfamilie auf. Bereits als 14jähriger tritt er in die Kommunistische Jugend Deutschlands ein. Aus der KPD wird er 1928 wegen seines Eintretens für eine Einheitsfront von Sozialdemokraten und Kommunisten ausgeschlossen. 1933 wird er als Rechtsreferendar aus politischen Gründen entlassen und berät seitdem viele Regimegegner juristisch. Nach seiner ersten Verhaftung emigriert Abendroth in die Schweiz, wo er auch promoviert. Nach längerer Kuriertätigkeit entschließt er sich 1935 zur Rückkehr nach Berlin. Hier betätigt er sich aktiv bis zu seiner mehrjährigen Inhaftierung 1937 im Widerstand. Seit Februar 1943 Soldat einer der "Bewährungseinheiten" 999 desertiert er im September 1944 zur griechischen Partisanenorganisation ELAS. Nach seiner Gefangennahme durch die Engländer beteiligt er sich an der politischen Aufklärungsarbeit von Regimegegnern in Kriegsgefangenschaft.')
