# -*- coding: utf-8 -*- 

"""
Convert CoNLL output of parzu/corzu to HTML
"""

import sys, re, random, colorsys

def gen_color(colors):
    """ Generate color hex """
    while True:
        colorcode=random.randint(0, 16777215)
        color='#%x'%colorcode
        if not color in colors:
            return color


def convert_to_text(name: str) -> str:
    dok=open('tmp/test.txt.coref','r').read()
    lines=dok.split('\n')
    ids=[]
    coref={}    # colors dict
    coref_ids=[]
    sent=0

    # Collect all coreference ids
    for line in lines:
        if not line=='':
            if not line.strip().endswith('_') and not line.strip().endswith('-'):
                l=line.split('\t')
                ms=l[-1].split('|')
                for m in ms:
                    if not m=='': ids.append(re.search('\d+',m).group())
    text = ""
    synonym = ""
    sentence = ""
    del_list = []
    for line in lines:

        if line=='\n' or line =='\t\n' or line=='':
            text += sentence + '\n'
            sentence = ""
            sent+=1

        else:
            index = lines.index(line)
            line=re.split('\t| +',line)

            # No coreference
            if line[-1].strip()=='_' or line[-1].strip()=='-':
                sentence += line[1] + " "


            else:
                coref_start=re.findall('\(\d+',line[-1])
                coref_start.reverse()
                for id in coref_start:
                    idint=re.search('\d+',id).group()
                    if len([x for x in ids if x==idint])>1: # no singeltons
                        # Finde vollständige Koreferenz
                        coref_string = ""
                        tmp_index = index
                        if '(' in lines[tmp_index].strip().split('\t')[-1]:
                            while not ')' in lines[tmp_index].strip().split('\t')[-1]:
                                coref_string += lines[tmp_index].strip().split('\t')[1] + " "
                                tmp_index += 1
                            coref_string += lines[tmp_index].strip().split('\t')[1]
                        if name in coref_string:
                            coref_string = name

                        coref_ids.append(id)
                        if id in coref and id not in del_list:   # open cset
                            if coref[id] not in sentence and coref[id] not in coref_string:
                                line[1] = coref[id]
                                synonym = 'y'
                        else:                   # new cset
                            if coref and id not in del_list:
                                # Synonymauflösung
                                vergleich_string = ""
                                if '(' in lines[index].strip().split('\t')[-1]:
                                    while not ')' in lines[index].strip().split('\t')[-1]:
                                        vergleich_string += lines[index].strip().split('\t')[1] + " "
                                        index += 1
                                    vergleich_string += lines[index].strip().split('\t')[1]
                                start_id = list(coref.keys())[0]
                                synonym = input(f"Sind '{coref[start_id]}' (ID: '{start_id}') und '{vergleich_string}' (ID: '{id}') dieselbe Person? (y/n)")
                                if synonym == 'y':
                                    coref[id] = coref[start_id]
                                elif synonym == 'n':
                                    delete = input(f"Soll '{vergleich_string}' weiter berücksichtigt werden?(y/n)")
                                    if delete == 'y':
                                        coref[id] = vergleich_string
                                    else:
                                        del_list.append(id)

                            else:
                                coref[id] = coref_string
                # Insert word ID here?
                sentence += line[1] + " "
                coref_end=re.findall('\d+\)',line[-1])
                for id in coref_end:
                    idint=re.search('\d+',id).group()
                    if len([x for x in ids if x==idint])>1:
                        last_id=coref_ids.pop()
                        #if coref[last_id] != line[1] and not synonym == 'y':
                        #    coref[last_id] += line[1]
    print(text)
    return text
