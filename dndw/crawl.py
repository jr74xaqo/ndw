import re

import json
from bs4 import BeautifulSoup
from urllib.request import urlopen


def crawl_category(subcat_url):
    pers_list = []
    id_list = []
    all_persons_dictionary = {}
    html = urlopen(subcat_url)
    soup = BeautifulSoup(html, "lxml")
    subcategory_container = soup.find(id='mw-subcategories')
    if subcategory_container:
        subcategories = subcategory_container.find_all("a")
        for subcategory in subcategories:
            subcat_url = subcategory.get("href")
            if subcat_url.startswith("/wiki"):
                all_persons_dictionary.update(crawl_category("https://de.wikipedia.org"+subcat_url))
    persons_container = soup.find(id='mw-pages')
    persons = persons_container.find_all("a")
    counter = 0
    for person in persons:
        person_url = person.get("href")
        if "Liste" in person.contents[0]:
            continue
        if "vorherige Seite" in person.contents[0]:
            continue
        if "nächste Seite" in person.contents and counter != 1:
            counter = 1
            continue
        elif "nächste Seite" in person.contents and counter == 1:
            crawl_category("https://de.wikipedia.org"+person_url)
        pers_dict = (crawl_person("https://de.wikipedia.org"+person_url))
        all_persons_dictionary.update({pers_dict['ID']: pers_dict})
        print(pers_dict['ID'])
    return all_persons_dictionary


def crawl_person(person_url):
    html = urlopen(person_url)
    soup = BeautifulSoup(html, "lxml")
    head = soup.find("head")
    script = head.find('script')
    script = script.contents[0].replace("\n", "").replace('"', "")
    categories = re.findall(r'(?<=wgCategories:\[)[\w\s,.äöü()\d"-–:„“’]+(?=\])', script)
    categories = categories[0].split(",")
    wikidata_id = re.findall(r'(?<=wgWikibaseItemId:)(Q\d+)', script)
    name = re.findall(r'(?<=wgTitle:)([\w+\s-]+)', script)
    return {"Name": name[0], "ID": wikidata_id[0], "Kategorien": categories}




def find_all_persons():
    jsonInput = json.dumps(crawl_category('https://de.wikipedia.org/w/index.php?title=Kategorie:Person_(Widerstand_gegen_den_Nationalsozialismus)'))
    jsonFile = open("personen_wiki.json", "w")
    jsonFile.write(jsonInput)
    jsonFile.close()

find_all_persons()