import graph_builder
import random


def graph_stats():
    print("GRAPHSTATISTIKEN")
    graph_build = graph_builder.GraphBuilder("validation_connectionists_no_fine_tune_2022-08-16_graph")
    graph_build.graph_stats()

    graph_build.print_all_relations()


def coref_validator():
    print("VALIDATE REPLACEMENT")
    with open("data/validate_replacement_2022-03-02.txt", 'r', encoding='UTF-8') as f:
        lines = f.readlines()
    print(len(lines))
    max_lines = len(lines)*0.1
    counter = 0
    random.seed(1917)
    while counter < max_lines:
        #print(lines[random.randrange(0, len(lines)+1)])
        counter += 1

def keyserlingk_relations():
    with open("data/keyserlingk_relations.txt", 'r', encoding='UTF-8') as f:
        lines = f.read().splitlines()
    graph_build = graph_builder.GraphBuilder("rel_ent_collection_2022-03-02_graph")
    node_list = list(graph_build.graph.nodes)

    key_relations_counter_all = len(lines)
    key_relations_counter = len(lines)
    graph_relations_counter = 0
    all_persons = set()
    missing_persons = set()

    for line in lines:
        relation = line.split(" – ")
        key_name = relation[0].split(', ')
        key_name1 = relation[1].split(', ')
        name = f"{key_name[1]} {key_name[0]}"
        name1 = f"{key_name1[1]} {key_name1[0]}"
        all_persons.add(name)
        all_persons.add(name1)
        try:
            node = graph_build.name_dict[name]
        except KeyError:
            missing_persons.add(name)
            key_relations_counter -= 1
            continue

        try:
            node1 = graph_build.name_dict[name1]
        except KeyError:
            missing_persons.add(name1)
            key_relations_counter -= 1
            continue

        if node in node_list and node1 in node_list:
            if node1 in list(graph_build.graph[node]):
                graph_relations_counter += 1
        else:
            key_relations_counter -= 1
    print(f"Gesamtzahl Relationen: {key_relations_counter_all}")
    print(f"Gesamtzahl Relationen (ohne fehlende Personen): {key_relations_counter}")
    print(f"Anzahl gefundener Relationen: {graph_relations_counter}")
    print(f"Anzahl nicht gefundener Relationen: {key_relations_counter - graph_relations_counter}")
    print(f"Gesamtzahl Personen: {len(all_persons)}")
    print(f"Anzahl fehlender Personen: {len(missing_persons)}")

    remaining_persons = all_persons - missing_persons
    print(f"Anzahl übereinstimmender Personen: {len(remaining_persons)}")
    random.seed(1917)
    control_persons = set()
    while len(control_persons) < 11:
        control_persons.add(list(remaining_persons)[random.randrange(0, len(remaining_persons))])
    print(control_persons)

