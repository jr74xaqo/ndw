import json.decoder
import logging
import time

from classes import Organisation, Person, Location, Entity
import i_o

logger = logging.getLogger("dndw")

entity_collection = {}
entity_set = set()
relationship_collection = {}


def get_class(name: str, tag, force_init=False) -> Entity:
    # existiert Entität mit geg. Namen bereits?
    matches = [ent for ent in entity_set if name in ent.shorts]
    klasse = None
    if force_init or not matches:
        if tag == 'PER':
            klasse = Person(name)
            entity_collection[name] = klasse
        elif tag == 'LOC':
            klasse = Location(name)
            entity_collection[name] = klasse
        elif tag == 'ORG':
            klasse = Organisation(name)
            entity_collection[name] = klasse
    if matches:
        klasse = matches[0]  # Setzt 'klasse' auf die erste Übereinstimmung
    if len(matches) > 1:
        logger.warning('given name %s fits more than one stored entity %s', name, matches)
    return klasse



def wikification(entity: str, tag: str, api_result: list = None, waiter: int = 0, unwikificated: bool = False):
    api_result_dict = i_o.read_pkl("wikifications")
    try:
        api_result = api_result_dict[entity]
    except KeyError:
        if not api_result:
            import requests
            api_url = "https://rel.cs.ru.nl/api"
            document = {
                "text": entity,
                "spans": [(0, len(entity))]
            }
            try:
                api_result = requests.post(api_url, json=document).json()
                api_result_dict[entity] = api_result
                i_o.write_pkl(api_result_dict, "wikifications")
            except requests.exceptions.ConnectionError:
                time.sleep(waiter + 1)
                return wikification(entity, tag, waiter=waiter + 1)
            except json.decoder.JSONDecodeError:
                api_result = []

    # print(API_result)
    if len(api_result) == 1:
        wiki_name = api_result[0][3]
        if any(wiki_name.replace('_', ' ') == key for key in list(entity_collection.keys())):
            klasse = entity_collection[wiki_name.replace('_', ' ')]
            klasse.set_wikificated()
            klasse.add_shorts(wiki_name)
            return klasse
        elif any(wiki_name.replace(' ', '_') == key for key in list(entity_collection.keys())):
            klasse = entity_collection[wiki_name.replace(' ', '_')]
            klasse.set_wikificated()
            klasse.add_shorts(wiki_name)
            return klasse
        else:
            if tag == 'PER':
                klasse = Person(wiki_name)
                klasse.add_shorts(entity)
                klasse.set_wikificated()
                entity_collection[wiki_name] = klasse
                return klasse
            elif tag == 'LOC':
                klasse = Location(wiki_name)
                klasse.add_shorts(entity)
                klasse.set_wikificated()
                entity_collection[wiki_name] = klasse
                return klasse
            elif tag == 'ORG':
                klasse = Organisation(wiki_name)
                klasse.add_shorts(entity)
                klasse.set_wikificated()
                entity_collection[wiki_name] = klasse
                return klasse
    elif len(api_result) < 1:
        # ToDo: handle unwikificated classes
        if any(entity.replace("_", " ") == key for key in list(entity_collection.keys())):
            klasse = entity_collection[entity.replace("_", " ")]
            klasse.add_shorts(entity)
            return klasse
        elif any(entity.replace(" ", "_") == key for key in list(entity_collection.keys())):
            klasse = entity_collection[entity.replace(" ", "_")]
            klasse.add_shorts(entity)
            return klasse
        elif unwikificated:
            if tag == 'PER':
                klasse = Person(entity)
                entity_collection[entity] = klasse
                return klasse
            elif tag == 'LOC':
                klasse = Location(entity)
                entity_collection[entity] = klasse
                return klasse
            elif tag == 'ORG':
                klasse = Organisation(entity)
                entity_collection[entity] = klasse
                return klasse
        return None
    elif len(api_result) > 1:
        result_list = dict(enumerate(api_result, 1))
        wiki_data = i_o.load_json("wikification")
        for i in result_list:
            print(f'{i}\t{result_list[i]}')
        if entity in list(wiki_data.keys()):
            index = wiki_data[entity]
        else:
            index = int(
                input(f'Gib die Nummer des Ergebnis an, dem {entity} zugeordnet werden soll (0 = keine davon): '))
        if index == 0:
            wiki_data[entity] = index
            i_o.write_json(wiki_data, "wikification")
            return None
        else:
            try:
                correct_result = [result_list[index]]
                wiki_data[entity] = index
                i_o.write_json(wiki_data, "wikification")
                return wikification(entity, tag, correct_result)
            except KeyError:
                print('Fehlerhafte Eingabe, bitte erneut versuchen')
                return wikification(entity, tag)
