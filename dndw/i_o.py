import json
import pickle
import logging
import os
from flair.models import SequenceTagger, RelationExtractor

logger = logging.getLogger("dndw")

filepath = os.path.dirname(__file__)
models_base_path = os.path.join(filepath, 'models', 'relation_extraction')

def load_models(no_coref, neural_rel_ex):
    model_list = []
    # Definiere den relativen Pfad zu den Modellen
    if neural_rel_ex:
        if no_coref:
            ner_tagger = SequenceTagger.load("de-ner")
            # Modellpfad für das relation_extractor-Modell
            model_path = os.path.join(models_base_path, 're-100-no_coref', 'final-model.pt')
        else:
            model_path = os.path.join(models_base_path, 're-500', 'final-model.pt')
            ner_tagger = SequenceTagger.load("de-ner")
        # Überprüfe und lade das relation_extractor-Modell
        if os.path.isfile(model_path):
            relation_extractor: RelationExtractor = RelationExtractor.load(model_path)
        else:
            raise FileNotFoundError(f"Model not found in {model_path}")

        model_list.extend((ner_tagger, relation_extractor))
    else:
        ner_tagger = SequenceTagger.load("de-ner-large")
        pos_tagger = SequenceTagger.load("de-pos")
        model_list.extend((pos_tagger, ner_tagger))

    return model_list

def load_json(file_to_load):
    logger.info(f"loading {file_to_load}.json")
    with open(os.path.join(filepath, 'data', f"{file_to_load}.json"), "r", encoding="UTF-8") as read_file:
        return json.load(read_file)


def write_pkl(collection, filename):
    with open(os.path.join(filepath, 'data', f"{filename}.pkl"), 'wb') as file:
        pickle.dump(collection, file)
        file.close()
    logger.info(f"data saved in {filename}.pkl")


def read_pkl(filename):
    logger.info(f"loading {filename}.pkl")
    try:
        with open(os.path.join(filepath, 'data', f"{filename}.pkl"), 'rb') as file:
            relation_collection = pickle.load(file)
            file.close()
            return relation_collection
    except FileNotFoundError:
        logger.error(f"No file {filename}.pkl found.")
        return {}


def write_json(data,filename):
    """Speichere Ergebnisse"""
    with open(os.path.join(filepath, 'data', f"{filename}.json"), "w", encoding="UTF-8") as f:
        json.dump(data, f, ensure_ascii=False, indent=4)
    logger.info(f"data saved in {filename}.json")


def write_repl_valid(sent, name, filename):
    with open(os.path.join(filepath, 'data', f"{filename}.tt"), "a+", encoding="UTF-8") as f:
        f.write(name + '\t' + sent + '\n')
    logger.info(f"data saved in {filename}.txt")
