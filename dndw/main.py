import argparse
import logging
from datetime import date

import networkx as nx



def main():
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger("dndw")
    ap = argparse.ArgumentParser()
    ap.add_argument("-ie", "--extract_information", required=False, action="store_true", help="Flag for information extraction pipeline")
    ap.add_argument("-gr", "--graph", required=False, action="store_true", help="Flag to activate graph builder")
    ap.add_argument("-pre", "--preprocess", required=False, action="store_true", help="Flag to run preprocesser for the given file")
    ap.add_argument("-ui", "--user_interface", required=False, action="store_true", help="Flag to run user interface")
    ap.add_argument("-ev", "--evaluation", required=False, action="store_true", help="Flag to run evaluation")
    ap.add_argument("-f", "--file", required=False, type=str, help="Specify the file name")
    ap.add_argument("-t", "--testing", required=False, action="store_true", help="Test the pipeline only on the first entry of the dataset")
    ap.add_argument("-uw", "--unwikificated", required=False, action="store_true", help="Specify the file name")

    args = ap.parse_args()
    ie = args.extract_information
    gr = args.graph
    pre = args.preprocess
    ui = args.user_interface
    ev = args.evaluation
    file = args.file
    testing = args.testing
    unwikificated = args.unwikificated
    if ie:
        import information_extraction
        #information_extraction.extract_information("rel_ent_collection")
        filename = f"{file}_{date.today()}"
        information_extraction.extract_information(filename, testing, unwikificated)
    if gr:
        import graph_builder
        #graph = graph_builder.build_graph("rel_ent_collection")
        g = graph_builder.GraphBuilder()
        g.build_graph(file)
        #g.build_graph("rel_ent_collection")

        # nx.write_gpickle(g.get_graph(), f"data/{file}_graph.pkl")
    if pre:
        from preprocessing import Preprocess
        Preprocess("attempt9")
        pass
    if ui:
        import dash_app as app
        #import request_handler
        #graph_builder = graph_builder.GraphBuilder(f"{file}_graph")
        #rh = request_handler.App(graph_builder)

        #pers_list = [{'label':pers, 'value': pers} for pers in list(graph_builder.name_dict.keys()) if isinstance(graph_builder.name_dict[pers], classes.Person)]
        #graph.load_graph("test_abendroth_graph")
        #rh = app.app_runner().App(graph_builder)
        #app.init_request_handler(rh)
        #request_handler.App(graph)
    if ev:
        import evaluation
        evaluation.graph_stats()
        #evaluation.coref_validator()
        #evaluation.keyserlingk_relations()






if __name__ == '__main__':
    main()


