import glob
import dash
from dash import dcc
from dash import html
from dash import callback_context
from dash.dependencies import Input, Output, State
import request_handler as rh
import i_o
from api_handler import NetworkGraph, NetworkNode, NetworkEdge, NetworkImage

from flask import Flask
from flask_restful import Api

LATEST_GRAPH_FILE = "rel_ent_collection_2024-10-28_graph"
server = Flask('dndw_server')

app = dash.Dash(__name__, title="DNDW", server=server)
# api = Api(server)





# api.add_resource(NetworkGraph, '/network/<node>', '/network')
# api.add_resource(NetworkNode, '/node/<node>', '/node')
# api.add_resource(NetworkEdge, '/edge/<node>?<neighbor>')
# api.add_resource(NetworkImage, '/image')

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options

#graph_build = graph_builder.GraphBuilder("test_abendroth_graph")


graph_files = [graph_file.replace(".pkl", "").strip("data/") for graph_file in glob.glob("data/*_graph.pkl")]

pers_list = i_o.load_json("name_list")

# request_handler = rh.App(LATEST_GRAPH_FILE)


app.layout = html.Div(
    children=[
        html.H1(children='Das Netzwerk des Widerstands'),

        html.Center(
            children=[
                html.P(
                    children=[
                        '''Das Netzwerk des Widerstands is centered around resistance fighters against the Nazi-regime.
                                            It is the result of a NLP-pipline extracting Named Entities and the relations 
                                            between them and transferring those into a knowledge-graph structure.
                                            The resulting graph provides information about the structure of the resistance
                                            network. On this website this network can be explored in detail.
                                            Currently the graph is based on the ''',
                        html.A(
                            href="https://www.gdw-berlin.de/vertiefung/biografien/personenverzeichnis/",
                            children="collection of short Biographies by Gedenkstätte Deutscher Widerstand",
                        ),
                        '''.'''
                    ],
                ),
                html.P(
                    children=[
                        html.H2("Instructions"),
                        html.Ol(
                            children=[
                                html.Li(children=["Check out the ", html.A("tooltips", href='#tooltips'), " to get an overview how this site works."]),
                                html.Li("At first set filters and search depth."),
                                html.Li("Decide for a search method"),
                                html.Li("If you want to refine your search, change the search parameters then click the same search button again."),
                                html.Li("You can reset all field by clicking on whole graph again.")

                            ]
                        ),
                    ]
                )
            ]

        ),
        html.Div(
            id='filter_select',
            children=[
                html.Div(
                    children=[
                        html.Label("Filter"),
                        dcc.Checklist(
                            id='filter',
                            options=[
                                {'label': 'Person', 'value': 'PER'},
                                {'label': 'Location', 'value': 'LOC'},
                                {'label': 'Organisation', 'value': 'ORG'},
                                {'label': 'Event', 'value': 'DATE'},
                            ],
                        )
                    ],
                ),
                html.Div(
                    children=[
                        html.Label("Suchtiefe"),
                        dcc.Slider(
                            min=1,
                            max=4,
                            step=None,
                            marks={1: '1', 2: '2', 3: '3', 4: '4'},
                            id='depth',
                            value=1
                        )
                    ]
                ),
            ]
        ),
        html.Table(
            id='suche',
            children=[
                html.Tbody([
                    html.Tr(
                        children=[
                            html.Td(html.Label("Knotensuche",)),
                            html.Td(dcc.Input(id="knotensuche", type='text', value='')),
                            html.Td(html.Button('Suche Knoten', id='submit_val_node', n_clicks=0)),
                        ]
                    ),

                    html.Tr(
                        children=[
                            html.Td(html.Label("Relationensuche")),
                            html.Td(dcc.Input(id="relationensuche", type='text', value='')),
                            html.Td(html.Button('Suche Relation', id='submit_val_relation', n_clicks=0))
                        ]
                    ),

                    html.Tr(
                        children=[
                            html.Td(html.Label("Pfadsuche")),
                            html.Td(dcc.Input(id="pfadsuche1", type='text', value='')),
                            html.Td(html.Button('Suche Pfad zwischen angegebenen Knoten', id='submit_val_path', n_clicks=0))
                        ]
                    ),
                ]
                ),
            ]
        ),

        html.Div(
            children=[
                html.Label('Personenauswahl'),
                dcc.Dropdown(
                    id = 'personenwahl',
                    value=pers_list[0],
                    options=pers_list,
                ),
            ]
        ),
        html.Div(
            children=[
                html.Button('Gesamter Graph', id='submit_whole_graph', n_clicks=0)
            ]
        ),

        dcc.Graph(id='dndw_graph',
                  style={"height": 'auto',
                         "width":'100%',
                         #'max-width': 1000
                         }
                  ),

        html.Div(
            children=[
                html.Label("""Textquelle der Relation"""),
                dcc.Markdown(id='hover-data', style={})
            ]
        ),
        html.H2("Tooltips", id='tooltips'),
        html.Dl(
            children=[
                html.H3("Search parameters"),
                html.Dt("Filter"),
                html.Dd("In the resulting graph include only these nodes and edges with the specified tag."),
                html.Dt("Personen"),
                html.Dd("Only include nodes tagged as person"),
                html.Dt("Location"),
                html.Dd("Only include nodes tagged as location"),
                html.Dt("Organisation"),
                html.Dd("Only include nodes tagged as Organisation"),
                html.Dt("Events"),
                html.Dd(
                    "Only include EDGES tagged as Events. This includes only those relations where a date could be retrieved"),
                html.Dt("Suchtiefe"),
                html.Dd("Set the depth of the search. At search depth 2 all neighbors of all neighbors are included too."),
                html.H3("Search methods"),
                html.Dt("Knotensuche:"),
                html.Dd(
                    "Search for the given node in the graph and outputs the node and all its neighbors."),
                html.Dt("Relationensuche"),
                html.Dd(
                    "Search for the given term in all relations, outputs every relation and its corresponding nodes including the given term."),
                html.Dt("Pfadsuche:"),
                html.Dd(
                    "Input a comma-separated list of 2 nodes or more, the result is shortest path between given nodes"),
                html.Dt("Personenauswahl:"),
                html.Dd("Choose one of over 600 resistance fighters and get their individual graph"),
            ]
        ),
        html.Div(
            id="choose_graph",
            children=[
                html.Label("Graphauswahl"),
                dcc.Dropdown(
                    id='graph_wahl',
                    value=LATEST_GRAPH_FILE,
                    # DEBUG GRAPH
                    # value='playground_graph',
                    options=graph_files,
                )
            ]
        ),
        html.Footer(
            children=["A project by ",
                      html.A("Jonas Richter", href='mailto:jr74xaqo+dndw@uni-leipzig.de'),
                      html.Br(),
                      "With thanks to: Sandra Bernstein, Richard Aude, Nils Wenzlitschke and Viet Dung Le."""
                      ]
        )
    ],
)


@app.callback(Output('dndw_graph', 'figure'),
              Output('knotensuche', 'value'),
              Output('relationensuche', 'value'),
              Output('pfadsuche1', 'value'),
              Output('personenwahl', 'value'),
              Input('personenwahl', 'value'),
              Input('submit_val_node', 'n_clicks'),
              Input('submit_val_path', 'n_clicks'),
              Input('submit_val_relation', 'n_clicks'),
              Input('submit_whole_graph', 'n_clicks'),
              State('depth', 'value'),
              State('filter', 'value'),
              State('knotensuche', 'value'),
              State('pfadsuche1', 'value'),
              State('relationensuche', 'value'),
              State('graph_wahl', 'value'),
              )
def update_graph(person:str, click_node:int, click_path:int, click_relation:int, click_graph:int,
                 depth:int, filtr:list, knoten:str, path1:str, relation:str, graph_file:str):
    """

    """
    changelist = [p['prop_id'] for p in callback_context.triggered]
    changed_id = changelist[0]
    node = None
    request_handler = rh.App(graph_file)

    if 'personenwahl.value' in changed_id:
        node = person
    elif 'submit_val_relation' in changed_id and click_relation > 0:
        if knoten:
            node = knoten
        else:
            node = ""
        if not relation:
            relation = ""
    elif 'submit_whole_graph' in changed_id and click_graph > 0:
        node = ""
    elif 'submit_val_node' in changed_id and click_node > 0:
        node = knoten
    # Titlegenerator
    title = node
    if 'submit_val_path' in changed_id and click_path > 0:
        node = path1.split(', ')
        if len(node) < 2:
            node = path1.split(',')
        title = ", ".join(node)
        title = 'Pfad ' + title

    if node:
        """Codeblock to generate Graph and Subgraph"""
        request_handler.create_subgraph(node=node, filtr=filtr, depth=depth)

        """latest search is needed to use the search depth"""
        return [request_handler.make_plotly(title, True), knoten, relation, path1, person]

    else:
        return [request_handler.make_plotly("DNDW", False), '', '', '', '']


def recently_clicked(button:str):
    """
    Helper function to determine which button was clicked most recently.
    Needed for the search depth bar to work properly.
    """
    last_clicked = button
    return last_clicked


@app.callback(
    Output('hover-data', 'children'),
    Input('dndw_graph', 'hoverData')
)
def hover_relation(hoverData):
    try:
        return hoverData['points'][0]['hovertext']
    except (KeyError, TypeError):
        return "No Data"


app.run_server(debug=False)
