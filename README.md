# Netzwerk des Widerstands - DNDW
This application is a pipline able to recognise Named Entities and their relations from written text and transform them into a knowledge graph.
The NLP processes are based on [flair](https://github.com/flairNLP/flair).
The results can be seen in the dash-app.


## Installation
After cloning the repository navigate into the repository and run:
```shell
python -m venv .venv/

source .venv/bin/activate

pip install -r requirements.txt
```

These commands will create a new Python Virtual Environment and install the needed dependencies for this project.

## Running the web application
After the installation you can run the web application:
```shell
python dndw/main.py -ui
```

## License
